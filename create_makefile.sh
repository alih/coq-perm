#! /bin/sh

# replacing crazy Coq's makefile $(shell) with a harmless $(strip)
# because those shell calls can fail and their result is not used anyway
coq_makefile -no-install *.v | sed -e 's/shell/strip/' >Makefile || exit 1
