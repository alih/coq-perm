(* Author: Ilya Mezhirov
 * License: CC0
 *)
(** * This file contains some rewrite lemmas that shouldn't be necessary but for some reason they come in handy.
 
 * This file may not import anything except Coq libraries.
 *) 

Require Import Arith.

Section Basics.

Variable A: Type.
Hypothesis eq_dec : forall x y : A, {x = y}+{x <> y}.

Theorem if_eq:
  forall Q: Type,
  forall x:A,
  forall a b:Q,
    (if eq_dec x x then a else b) = a.
intros.
destruct (eq_dec x x). trivial. tauto. Qed. 

Theorem if_not_eq:
  forall Q: Type,
  forall x y:A,
  forall a b:Q,
    x <> y -> 
      (if eq_dec x y then a else b) = b.
intros.
destruct (eq_dec x y). contradiction. trivial. Qed.

End Basics.

(* TODO: figure out rewrite hints *)

(* vim: set ft=coq: *)
