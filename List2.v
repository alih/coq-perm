(* Author: Ilya Mezhirov
 * License: CC0
 *)


(** * This is the theory of index(), a simple linear search.
 
 This file was called Index.v, but that upsets coqdoc, so it's List2 now.   
 *)

Require Import List List1.
Require Import MinMax.
Require Import Arith.
Require Import Utils.

Section Index.

Variable A: Type.
Hypothesis eq_dec : forall x y : A, {x = y}+{x <> y}.

(** Search the list for the first occurence of the given element.
  Return its index (wrapped in Some) or None if not found.
 *)
Fixpoint index a (l: list A): option nat :=
  match l with
  | nil => None
  | h :: t => if eq_dec h a then Some 0 else 
                match index a t with
                | Some i => Some (S i)
                | None => None
                end
  end.

Lemma theindex_helper (a: A) (l: list A): (* Yeah, I know I could just use 0. *)
  In a l -> l = nil -> nat.
intros al lnil.
rewrite lnil in al.
simpl in al. contradiction. Qed.

(** A version of index that needs a proof that the item is there. *)
Program Fixpoint theindex (a: A) (l: list A) (c: In a l): nat := (* TODO: rename index to something and this to index *) 
  match l with
  | nil => theindex_helper a l c _
  | h :: t => if eq_dec h a then 0 else S (theindex a t _)
  end.
Next Obligation.
simpl in c.
destruct c.
contradiction. assumption. Qed.

Theorem theindex_irrel (a: A) (l: list A) (c1: In a l) (c2: In a l):
  theindex a l c1 = theindex a l c2.
induction l. simpl in c1. contradiction.
simpl. destruct (eq_dec a0 a). trivial.
rewrite IHl with (c2 := (theindex_obligation_2 a (a0 :: l) c2 a0 l (eq_refl (a0 :: l)) n)). (* WHY DO I HAVE TO DO THIS??? *)
trivial. Qed. 

Theorem index_theindex (a: A) (l: list A) (c: In a l):
  index a l = Some (theindex a l c).
induction l. simpl in c. contradiction.
simpl. destruct (eq_dec a0 a). trivial.
simpl in c. destruct c. contradiction.
rewrite IHl with (c:=(theindex_obligation_2 a (a0 :: l) (or_intror (a0 = a) i) a0 l
           (eq_refl (a0 :: l)) n)). (* really... *)
trivial. Qed.

(** Searching for something that's in there returns a successful result. *)
Theorem index_in:
    forall k l,
        In k l -> exists n: nat, index k l = Some n.
induction l.
intros. unfold In. contradiction.
intros. unfold In in H. destruct H. exists 0. simpl. rewrite H.
destruct eq_dec. trivial.
tauto.
apply IHl in H. destruct H.
exists (if eq_dec a k then 0 else S x).
unfold index. destruct eq_dec. trivial. fold index. rewrite H. trivial. Qed.

Theorem theindex_index (a: A) (l: list A) (c: In a l) (d: nat):
  theindex a l c = match index a l with Some n => n | None => d end.
rewrite index_theindex with (c:=c). trivial. Qed.

(** All the indices are less than the list length. *)
Theorem index_bound:
    forall a l,
        forall n, index a l = Some n -> n < length l.
induction l.
intros. unfold index in H. discriminate H.
intros. unfold index in H. destruct eq_dec.
injection H. intros. rewrite<- H0. unfold length. auto with *.
fold index in H.
unfold length.
destruct (index a l).
injection H. intros. clear H. rewrite<- H0.
assert(n1 < length l).
apply IHl. trivial.
auto with *.
discriminate H. Qed.

Theorem theindex_bound (a: A) (l: list A) (c: In a l):
  theindex a l c < length l.
induction l.
simpl in c. contradiction.
simpl. destruct eq_dec. auto with arith.
cut(forall c : In a l, S(theindex a l c) < S (length l)).
trivial.
auto with arith. Qed.

(** The initial segment before the found item is free of that item. *)
Theorem index_initial_segment:
  forall a x,
    ~ In x (match index x a with
            | Some n => firstn n a
            | None => a
            end).
induction a.
simpl. unfold not. trivial.
simpl. intro. destruct (eq_dec a x).
simpl. unfold not. trivial.
assert (~In x match index x a0 with
             | Some n => firstn n a0
             | None => a0
             end).
apply IHa.
remember (index x a0) as i.
destruct i.
simpl.
tauto.
simpl.
tauto. Qed.

(** Same as index_initial_segment, but checks that the item is there. *)
Theorem index_initial_segment_some:
  forall a x n,
    index x a = Some n ->
    ~ In x (firstn n a).
intros.
assert(~ In x (match index x a with
            | Some n => firstn n a
            | None => a
            end)). apply index_initial_segment. rewrite H in H0. assumption. Qed.

Theorem theindex_initial_segment x l (c: In x l):
   ~In x (firstn (theindex x l c) l).
rewrite theindex_index with (d:=0).
assert(exists n: nat, index x l = Some n).
apply index_in. assumption.
destruct H.
rewrite H.
apply index_initial_segment_some.
assumption.
Qed.

(** Something that's not found isn't in there. *)
Theorem index_none:
  forall a x,
    index x a = None <-> ~ In x a.
intros.
split.
(* right *)
assert(~ In x (match index x a with
            | Some n => firstn n a
            | None => a
            end)). apply index_initial_segment. intro. rewrite H0 in H. assumption.
(* left *)
induction a. simpl. trivial.
simpl. intro.
destruct (eq_dec a x).
destruct H. left. assumption.
assert(~In x a0).
tauto.
apply IHa in H0.
rewrite H0. trivial.
Qed.

(** Searching for the n-th element is successful and the index doesn't exceed n. *)
Theorem index_nth:
  forall a n d,
    n < length a ->
      exists k, Some k = index (nth n a d) a /\ k <= n.
induction a.
simpl.
intros.
apply Lt.lt_n_O in H. contradiction.
intros.
simpl in H.
assert(n > 0 -> n = S (pred n)).
apply Lt.S_pred.
assert(n <= 0 \/ 0 < n).
apply Lt.le_or_lt.
destruct H1.
assert(n = 0).
auto with arith.
rewrite H2.
simpl. rewrite if_eq. exists 0. split. trivial. trivial.
apply H0 in H1.
clear H0.
rewrite H1.
simpl.
specialize IHa with (n := pred n) (d := d).
rewrite H1 in H.
assert(pred n < length a0).
auto with arith.
apply IHa in H0.
destruct H0.
exists (if eq_dec a (nth (pred n) a0 d) then 0 else S x).
destruct H0.
rewrite<- H0.
split.
destruct (eq_dec a (nth (pred n) a0 d)). trivial. trivial.
destruct (eq_dec a (nth (pred n) a0 d)). auto with arith. auto with arith. Qed.

(** Looking up the found element by its index produces what we were searching for provided that it's there at all. *) 
Theorem nth_index:
  forall a x,
    match index x a with
    | Some n => forall d, nth n a d = x
    | None => ~In x a
    end.
induction a.
simpl. unfold not. trivial.
intros. simpl. destruct (eq_dec a x). tauto.
assert(match index x a0 with
       | Some n => forall d : A, nth n a0 d = x
       | None => ~ In x a0
       end). apply IHa.
remember (index x a0) as i.
destruct i. assumption.
tauto. Qed.

Theorem nth_theindex (a: A) (l: list A) (c: In a l) (d: A):
  nth (theindex a l c) l d = a.
rewrite theindex_index with (d:=0).
induction l. simpl in c. contradiction.
simpl. destruct eq_dec. assumption.
simpl in c. destruct c. contradiction.
remember (index a l) as i.
destruct i. apply IHl. assumption.
symmetry in Heqi.
rewrite index_none in Heqi. contradiction. Qed.

Theorem ith_theindex (a: A) (l: list A) (c: In a l):
  ith A (theindex a l c) l (theindex_bound a l c) = a.
rewrite ith_nth with (d:=a). apply nth_theindex. Qed.

(** Same as nth_index with a check. *)
Theorem nth_index_some:
  forall a x n,
    index x a = Some n -> forall d, nth n a d = x.
intros.
assert(match index x a with
    | Some n => forall d, nth n a d = x
    | None => ~In x a
    end). apply(nth_index). rewrite H in H0. apply H0. Qed.

(** Searching for the n-th element returns n when the element is not in the initial segment. *)
Theorem index_from_initial_segment:
  forall n a d,
    n < length a ->
    ~ In (nth n a d) (firstn n a) ->
      index (nth n a d) a = Some n.
intros.
remember (nth n a d) as x.
assert(~ In x (match index x a with
            | Some n => firstn n a
            | None => a
            end)).
apply index_initial_segment.
assert(In x a).
rewrite Heqx.
apply nth_In. assumption.
remember (index x a) as i.
destruct i.
Focus 2.
contradiction.
assert(forall d, nth n0 a d = x).
apply nth_index_some. symmetry. assumption.
assert((firstn n a) ++ (skipn n a) = a).
apply(firstn_skipn).
assert((firstn n0 a) ++ (skipn n0 a) = a).
apply(firstn_skipn).
assert(skipn n a = (nth n a d) :: (skipn (S n) a)).
apply skipn_decompose. assumption.
assert(skipn n0 a = (nth n0 a d) :: (skipn (S n0) a)).
apply skipn_decompose. apply index_bound with (a:=x). symmetry. assumption.
rewrite H6 in H4. rewrite H7 in H5.
assert(firstn n a = firstn n0 a /\ nth n a d = nth n0 a d /\ skipn (S n) a = skipn (S n0) a).
apply(list_eq_by_mutual_bound).
rewrite H4. rewrite H5. trivial. rewrite<- Heqx. assumption. rewrite H3. assumption.
destruct H8.
assert(length (firstn n a) = length(firstn n0 a)).
rewrite H8. trivial. rewrite_all firstn_length. 
assert(MinMax.min n (length a) = n).
apply(MinMax.min_l). auto with arith. rewrite H11 in H10.
assert(MinMax.min n0 (length a) = n0). apply(MinMax.min_l). 
assert(n0 < length a).
apply index_bound with (a:=x). symmetry. assumption. auto with arith.
rewrite H12 in H10. rewrite H10. trivial. Qed.

(** Absense of an element in the initial segment puts a lower bound on its index. *)
Theorem index_lower_bound:
  forall x a n i,
    ~ In x (firstn n a)
    -> index x a = Some i 
      -> i >= n.
intros.
assert(~ In x (match index x a with
            | Some k => firstn k a
            | None => a
            end)).
apply index_initial_segment.
rewrite H0 in H1.
assert(forall d, nth i a d = x).
apply nth_index_some. assumption.
assert(forall k, k > i -> In x (firstn k a)).
intros.
assert(forall d, nth i (firstn k a) d = nth i a d).
intros.
symmetry.
rewrite firstn_nth with (n := k). trivial. apply H3.
rewrite<- H2 with (d:=x).
rewrite firstn_nth with (n:=k).
apply nth_In.
rewrite firstn_length.
apply min_glb_lt.
apply H3.
apply index_bound with (a:=x). exact H0. apply H3.
assert(i < n \/ i >= n).
assert(i >= n \/ i < n).
apply Lt.le_or_lt.
tauto.
destruct H4.
apply H3 in H4.
contradiction.
assumption. Qed.

End Index.

(* vim: set ft=coq: *)
