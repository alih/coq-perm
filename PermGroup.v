(* Author: Ilya Mezhirov
 * License: CC0
 *)

(** * Here we compose and invert permutations.
 *)

Require Import List List1 List2.
Require Import PermDef.
Require Import Arith.

(** ** Composition *)
Definition perm_compose (a b: list nat): list nat :=
  map (fun (x: nat) => nth x a x) b.


(** Permutations are equal if their items are equal.
  The difference with list_eq_by_nth is that here the defaults are indices.
 *)
Theorem perm_eq_by_nth:
  forall a b: list nat,
    length a = length b ->
    (forall i: nat, i < length a -> nth i a i = nth i b i) -> a = b.
intros.
apply list_eq_by_nth with (d := 0).
assumption.
intros.
specialize H0 with (i := i).
assert(nth i a i = nth i b i).
apply H0.
assumption.
assert(nth i a 0 = nth i a i).
apply nth_indep. assumption.
assert(nth i b i = nth i b 0).
apply nth_indep. rewrite<- H. assumption.
rewrite H3. rewrite H2. rewrite H4. trivial. Qed.

(** The length of the composition is what it should be. *)
Theorem perm_compose_length:
  forall a b,
    length (perm_compose a b) = length b.
unfold perm_compose. intros. rewrite map_length. trivial. Qed.

(** Composition of permutations is a permutation. *) 
Theorem perm_compose_perm:
  forall a b,
    IsPermutation a -> IsPermutation b -> length a = length b -> IsPermutation (perm_compose a b).
intros.
unfold IsPermutation.
intros.
unfold perm_compose in H2.
rewrite map_length in H2.
unfold perm_compose.
remember (fun x : nat => nth x a x) as af.
rewrite<- H1 in H2.
apply H in H2.
assert(exists k: nat, k < length a /\ nth k a k = n).
  assert(exists k: nat, index nat eq_nat_dec n a = Some k).
  apply index_in. assumption.
  destruct H3.
  exists x.
  split.
  apply index_bound with (eq_dec := eq_nat_dec) (a := n). assumption.
  assert(
    match index nat eq_nat_dec n a with
    | Some x => forall d, nth x a d = n
    | None => ~In n a
    end).
  apply(nth_index).
  rewrite H3 in H4.
  apply H4.
destruct H3.
destruct H3.
assert(n = af x).
rewrite Heqaf.
symmetry.
assumption.
rewrite H5.
apply in_map.
apply H0.
rewrite H1 in H3. assumption. Qed.

(** The iota (identity permutation) is a permutation. *)
Theorem iota_perm:
  forall n,
    IsPermutation (iota n).
intros.
unfold IsPermutation. intros.
assert(nth n0 (iota n) 0 = n0).
apply iota_nth_in_range.
rewrite iota_length in H. assumption.
rewrite<- H0.
apply nth_In.
assumption. Qed.

(** Composition with identity from the left doesn't change the permutation. *)
Theorem iota_id_left:
  forall a,
    perm_compose (iota (length a)) a = a.
intros.
unfold perm_compose.
remember (fun x : nat => nth x (iota (length a)) x) as f.
assert(forall x, f x = x).
intro.
rewrite Heqf.
rewrite iota_nth. trivial.
clear Heqf.
induction a. simpl. trivial.
simpl.
rewrite IHa.
rewrite H with (x:=a).
trivial. Qed.

(** Composition with identity from the left doesn't change the permutation. *)
Theorem iota_id_right:
  forall a,
    perm_compose a (iota (length a)) = a.
intros.
unfold perm_compose.
apply perm_eq_by_nth.
rewrite map_length.
rewrite iota_length. trivial.
intros.
rewrite map_length in H.
remember (fun x : nat => nth x a x) as f.
assert(nth i (map f (iota (length a))) i = nth i (map f (iota (length a))) (f i)).
apply nth_indep.
rewrite map_length. assumption.
rewrite H0.
rewrite map_nth.
rewrite iota_nth_in_range.
rewrite Heqf.
trivial.
rewrite iota_length in H. 
assumption. Qed.

(** Composition is associative. 
 It's interesting that only c is required to be a permutation. *)
Theorem perm_compose_assoc:
  forall a b c,
    length b = length c ->
    IsPermutation c ->
    perm_compose (perm_compose a b) c = perm_compose a (perm_compose b c).
intros.
unfold perm_compose.
rewrite map_map.
apply perm_eq_by_nth.
rewrite_all map_length. trivial.
intros. rewrite_all map_length.
remember (fun x0 : nat => nth x0 a x0) as af.
remember (fun x : nat => nth x (map af b) x) as f1.
assert(nth i (map f1 c) i = nth i (map f1 c) (f1 i)).
apply nth_indep.
rewrite map_length.
assumption.
rewrite H2. rewrite map_nth. rewrite Heqf1.
remember (fun x : nat => nth (nth x b x) a (nth x b x)) as f2.
assert(nth i (map f2 c) i = nth i (map f2 c) (f2 i)).
apply nth_indep. rewrite map_length. assumption.
rewrite H3. rewrite map_nth. rewrite Heqf2.
remember (nth i c i) as ci.
remember (nth ci b ci) as bci.
remember (nth bci a bci) as abci.
assert(nth ci (map af b) ci = nth ci (map af b) (af ci)).
  apply nth_indep. rewrite map_length. rewrite H.
  apply perm_bound. assumption.
  assert(ci < length c).
  rewrite Heqci.
  apply perm_bound. assumption.
  apply nth_In. assumption. apply H0. assumption.
rewrite H4. rewrite map_nth. rewrite Heqaf. rewrite<- Heqbci. rewrite<- Heqabci. trivial. Qed.

(** Unpack the option with a default. 
 This is the OrElse operator, sometimes denoted ?: or ||. *)
Definition preferably (A: Type) (x: option A) (d: A) :=
  match x with
  | Some y => y
  | None => d
  end.

(** ** Inversion *)

(** A list that gives an identity when composed to something from the right
   is a permutation. *)
Theorem perm_if_invertible:
  forall a b n,
    length a = n ->
    length b = n ->
    (perm_compose a b) = (iota n) ->
    IsPermutation a.
unfold IsPermutation. intros.
rewrite H in H2.
unfold perm_compose in H1.
assert(In n0 (iota n)).
apply iota_upper_bound. assumption.
remember (fun x : nat => nth x a x) as f.
rewrite<-H1 in H3.
rewrite in_map_iff in H3.
destruct H3. destruct H3.
rewrite Heqf in H3.
rewrite<- H3. rewrite<- H3 in H2. clear H3.
apply nth_In. rewrite H.
assert(length a <= x \/ x < length a).
apply Lt.le_or_lt.
destruct H3.
assert (nth x a x = x). apply nth_overflow. assumption.
rewrite<- H5. assumption.
rewrite<- H. assumption.
Qed. 

(** Index of the n-th item of a permutation is n. *) 
Theorem perm_index_nth:
  forall p,
    IsPermutation p -> 
      forall d i,
        i < length p ->
          index nat eq_nat_dec (nth i p d) p = Some i.
intros.
assert(exists k, Some k = index nat eq_nat_dec (nth i p d) p /\ k <= i).
apply index_nth. assumption.
destruct H1. destruct H1.
assert(~In (nth i p d) (firstn i p)).
apply(perm_not_in_initial_segment). assumption. assumption.
apply(index_from_initial_segment). assumption. assumption. Qed.

(******************************************************************************)

(** Inverted permutation. *)
Definition perm_inv (p: list nat) : list nat :=
  map (fun x => preferably nat (index nat eq_nat_dec x p) x) (iota (length p)).

(** Inverting a permutation doesn't change its length. *)
Theorem inv_length:
  forall p, IsPermutation p -> length (perm_inv p) = length p.
intros. unfold perm_inv. rewrite map_length. rewrite iota_length. trivial. Qed.

(** The inverted permutation contains indices.
  This is pretty much the definition but simplified due to some range checks.
 *)
Theorem inv_nth:
  forall p,
  forall i: nat,
  forall d,
    IsPermutation p ->
    i < length p -> Some (nth i (perm_inv p) d) = index nat eq_nat_dec i p.
intros. unfold perm_inv. rewrite map_nth_in_range with (d':=d). Focus 2. rewrite iota_length. assumption.
rewrite iota_nth_in_range. Focus 2. assumption. 
remember (index nat eq_nat_dec i p) as j.
destruct j. simpl. trivial.
exfalso. assert(In i p).
apply H. assumption.
assert(exists n: nat, index nat eq_nat_dec i p = Some n).
apply(index_in). assumption.
destruct H2. rewrite<- Heqj in H2. discriminate H2. Qed.

(** Composing with the inverted from left gives identity. *)
Theorem inv_left:
  forall p,
    IsPermutation p -> perm_compose (perm_inv p) p = iota (length p).
intros. apply perm_eq_by_nth. rewrite perm_compose_length. rewrite iota_length. trivial. intros.
rewrite perm_compose_length in H0.
rewrite iota_nth_in_range. Focus 2. assumption.
unfold perm_compose.
remember (fun x : nat => nth x (perm_inv p) x) as f.
assert (nth i (map f p) i = nth i (map f p) (f i)).
apply nth_indep. rewrite map_length. assumption.
rewrite H1. rewrite map_nth. rewrite Heqf. remember (nth i p i) as pi.
unfold perm_inv.
assert(pi < length p).
  rewrite Heqpi.
  apply perm_bound. assumption. apply nth_In. assumption.
remember (fun x : nat => preferably nat (index nat eq_nat_dec x p) x) as g.
assert(nth pi (map g (iota (length p))) pi = nth pi (map g (iota (length p))) (g pi)).
apply nth_indep. rewrite map_length. rewrite iota_length. assumption.
rewrite H3.
rewrite map_nth. rewrite Heqg. unfold preferably.
rewrite iota_nth. rewrite Heqpi.
assert(index nat eq_nat_dec (nth i p i) p = Some i).
apply perm_index_nth. assumption. assumption.
rewrite H4. trivial. Qed.

(** Inverted permutation is a permutation. *)
Theorem inv_perm:
  forall p, IsPermutation p -> IsPermutation (perm_inv p).
intros.
apply perm_if_invertible with (a:=(perm_inv p)) (b:=p) (n:=length p).
apply inv_length. assumption. 
trivial.
apply inv_left. assumption.
Qed.

(** Composing with the inverted from right gives identity. *)
Theorem inv_right:
  forall p,
    IsPermutation p -> perm_compose p (perm_inv p) = iota (length p).
intros. apply perm_eq_by_nth. rewrite perm_compose_length. unfold perm_inv. rewrite map_length. trivial.
intros. rewrite perm_compose_length in H0. rewrite inv_length in H0.
rewrite iota_nth_in_range. Focus 2. assumption. Focus 2. assumption.
unfold perm_compose. rewrite map_nth_in_range with (d:=i) (d':=i). Focus 2. rewrite inv_length. assumption. assumption.
remember (nth i (perm_inv p) i) as j.
assert(j < length p).
  rewrite Heqj.
  assert(IsPermutation (perm_inv p)).
  apply inv_perm. assumption.
  remember (perm_inv p) as q.
  assert(forall k, In k q -> k < length q).
  apply perm_bound. assumption.
  assert(length p = length q).
  rewrite Heqq. symmetry. apply inv_length. assumption.
  rewrite H3. apply nth_prop_with_default. rewrite <- H3. assumption. assumption.
assert(Some j = index nat eq_nat_dec i p).
rewrite Heqj.
apply inv_nth. assumption. assumption.
apply nth_index_some with (eq_dec:=eq_nat_dec). symmetry. trivial. Qed.

(** Inverting a permutation twice doesn't change it. *)
Theorem inv_inv:
  forall p,
    IsPermutation p -> perm_inv (perm_inv p) = p.
intros.
assert (perm_compose p (perm_inv p) = iota (length p)).
apply inv_right. assumption.
assert (perm_compose (perm_inv (perm_inv p)) (perm_inv p) = iota (length (perm_inv p))).
apply inv_left. apply inv_perm. assumption.
rewrite inv_length in H1. Focus 2. assumption.
rewrite <- H0 in H1. clear H0.
remember (perm_inv p) as q.
assert (length p = length q) as len_pq. rewrite Heqq. symmetry. apply inv_length. assumption.
assert (IsPermutation q) as q_perm. rewrite Heqq. apply inv_perm. assumption.
assert (perm_compose (perm_compose (perm_inv q) q) p = perm_compose (perm_compose p q) p).
rewrite H1. trivial. 
rewrite perm_compose_assoc in H0. Focus 2. symmetry. assumption. Focus 2. assumption.
rewrite perm_compose_assoc in H0. Focus 2. symmetry. assumption. Focus 2. assumption.
assert(perm_compose q p = iota (length p)) as qp_id.
rewrite Heqq. apply inv_left. assumption.
rewrite qp_id in H0. rewrite iota_id_right in H0.
rewrite<- H0. 
rewrite len_pq. 
assert (length q = length (perm_inv q)) as len_qp. symmetry. apply inv_length. assumption. 
rewrite len_qp. rewrite iota_id_right. trivial. Qed.
 

(* vim: set ft=coq: *)
