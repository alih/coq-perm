(* Author: Ilya Mezhirov
 * License: CC0
 *)

(** * This is my support library complementing Coq's List.
 
 The code in this file closely follows the layout of Coq's List.
 *)

Require Import List.
Require Import MinMax.
Require Import Arith.

Require Import Utils.


(***********************************************************************)

(* Section Lists
 *    defines hd, hd_error, tl, In.
 *)

(***********************************************************************)

Section Facts.

Variable A: Type.
Hypothesis eq_dec : forall x y : A, {x = y}+{x <> y}.

(** Alternative definition of In through list construction. *) 
Theorem in_decompose:
  forall x: A,
  forall l: list A,
    In x l <-> exists a, exists b, l = a ++ x :: b.
induction l.
simpl. split. tauto. intro.
destruct H.
destruct H.
assert(length (nil: list A) = length (x0 ++ x :: x1)).
rewrite H. trivial.
rewrite app_length in H0. simpl in H0.
symmetry in H0.
remember (length x0) as n0.
remember (length x1) as n1.
assert(n0 + S n1 > 0).
assert(S n1 > 0).
auto with arith.
rewrite plus_comm.
auto with arith.
rewrite H0 in H1.
generalize H1.
apply lt_irrefl.
(* step *)
simpl.
split. intro x_in_al. destruct x_in_al. rewrite H.
exists nil. exists l. simpl. trivial.
rewrite IHl in H. destruct H. exists (a :: x0).
simpl. destruct H. exists x1. rewrite H. trivial.
(* right *)
intro E. destruct E. destruct H.
assert ({a = x} + {a <> x}). apply eq_dec.
case H0. tauto. intro a_ne_x. right. rewrite IHl.
assert (x0 <> nil).
  hnf. intro. rewrite H1 in H. simpl in H. inversion H. contradiction.
destruct x0. tauto. simpl in H. inversion H.
exists x0. exists x1. trivial. Qed.

(** Two items of a list that are not in initial segments of each other are one.
   In other words, two decompositions of the same list:
      a  x  b
   and 
      a' x' b'
   are the same decomposition if x' is not in a and x is not in a'.
 *)
Lemma list_eq_by_mutual_bound:
  forall a b a' b': list A, forall x x': A,
    a ++ x :: b = a' ++ x' :: b' 
   -> ~In x a' 
   -> ~In x' a
     -> a = a' /\ x = x' /\ b = b'.
induction a.
simpl. destruct a'. simpl. intros. split. trivial. split.
injection H. intros. assumption. 
injection H. intros. assumption.

simpl. intros. assert(a = x).
assert(hd a (x :: b) = hd a (a :: a' ++ x' :: b')).
rewrite H. trivial. simpl in H2. symmetry. assumption. 
tauto.
simpl. intros. destruct a'. simpl in H. injection H. intros. tauto.
rewrite<- app_comm_cons in H.
injection H. intros. rewrite_all<- H3.
assert(a0 = a' /\ x = x' /\ b = b').
apply IHa. assumption. simpl in H0. tauto. tauto.
destruct H4. rewrite H4. split. trivial. assumption. Qed.

(** A list of length 0 is empty. *)
Lemma list_empty_if_length_0:
  forall (l: list A),
    length l = 0 -> l = nil.
induction l.
simpl. trivial.
simpl. intro. discriminate H. Qed.

End Facts.

(***********************************************************************)

Section Elts.

Variable A: Type.

(** A variant of nth with a range check proof as an argument. *)
Lemma ith_helper:
  forall n (l: list A),
  n < length l -> l = nil -> A.
intros n l range lnil.
rewrite lnil in range. simpl in range. exfalso. assert(~(n < 0)). auto with arith. contradiction. 
Qed.

Program Definition ith (n: nat) (l: list A) (c: n < length l): A := match l with
  | nil => ith_helper n l c _
  | x :: _ => nth n l x
  end.

Theorem ith_nth:
  forall (n: nat) (l: list A) (c: n < length l) (d: A),
    ith n l c = nth n l d.
intros. destruct l.
simpl in c. assert(~ n < 0). auto with arith. contradiction.
simpl. destruct n. trivial. simpl in c. assert(n < length l).
auto with *.
apply nth_indep. assumption. Qed.

Theorem ith_in:
  forall (n: nat) (l: list A) (c: n < length l),
    In (ith n l c) l.
intros. rewrite ith_nth with (d:=(ith n l c)). apply nth_In. assumption. Qed.

Theorem ith_irrel:
  forall (i: nat) (a: list A) (p q: i < length a),
    ith i a p = ith i a q.
intros.
rewrite ith_nth with (d:=ith i a p).
rewrite<- ith_nth with (c:=q).
trivial. Qed.


(** A property that holds everywhere in the list holds for its particular item.
  This version checks for the index of the item to be in range.
 *)
Theorem nth_prop_in_range:
  forall a: list A,
  forall i: nat,
  forall P: A->Prop,
    i < length a ->
    (forall x: A, In x a -> P x) ->
    forall d: A, P (nth i a d).
intros a i P i_ok a_all d.
apply a_all.
apply nth_In. assumption.
Qed.

(** A property that holds everywhere in the list holds for its particular item.
  This version requires the default of nth to comply with the property also.
 *)
Theorem nth_prop_with_default:
  forall a: list A,
  forall i: nat,
  forall P: A->Prop,
  forall d: A,
    P d ->
    (forall x: A, In x a -> P x) ->
    P (nth i a d).
intros a i P d Pd a_all.
assert({In (nth i a d) a} + {nth i a d = d}).
apply nth_in_or_default.
case H. apply a_all. intro D. rewrite D. exact Pd. Qed.


(** A property that holds everywhere in the list holds for its particular item.
  This is the dependent type version.
 *)
Program Theorem ith_prop:
  forall a: list A,
  forall i: nat,
  forall P: A->Prop,
    i < length a ->
    (forall x: A, In x a -> P x) ->
      P (ith i a _).
intros. destruct a. simpl in H. assert(~ i < 0). auto with arith. contradiction. 
rewrite ith_nth with (d:=a).
apply nth_prop_with_default. apply H0. simpl. left. trivial.  assumption. Qed.

Hypothesis eq_dec : forall x y : A, {x = y}+{x <> y}.

(** Two lists of the same length and with the same items are equal. *)
Lemma list_eq_by_nth:
  forall a b: list A, forall d: A,
    length a = length b ->
    (forall i: nat, i < length a -> nth i a d = nth i b d) -> a = b.
induction a. intros.
simpl in H. symmetry. apply list_empty_if_length_0. symmetry. assumption.
induction b.
intros. 
simpl in H. discriminate H.
intros.
simpl in H. injection H. intro. clear H.
assert(a = a1).
  assert(a = nth 0 (a :: a0) d). simpl. trivial.
  assert(a1 = nth 0 (a1 :: b) d). simpl. trivial.
  rewrite H. rewrite H2. apply H0. simpl. auto with arith.
assert(a0 = b).
apply IHa with (d:=d). assumption.
simpl in H0. 
intros.
assert(S i < S (length a0)).
auto with arith.
specialize H0 with (i := S i).
apply H0 in H3. assumption.
rewrite H. rewrite H2. trivial. Qed.

(***********************************************************************)

(** ** Remove *)

(** Removing something from a list cannot increase its length. *)
Theorem remove_length:
  forall p,
    forall x: A,
        length(remove eq_dec x p) <= length p.
induction p.
intros. simpl. trivial.
intros. simpl. destruct (eq_dec x a).
assert (length (remove eq_dec x p) <= length p).
apply IHp.
auto.
simpl.
assert (length (remove eq_dec x p) <= length p).
apply IHp.
auto with *. Qed.

(** Removing an item from a list decreases its length if the item was in there. *)
Theorem remove_lowers_length:
  forall p,
    forall x: A,
        In x p -> length(remove eq_dec x p) < length p.
induction p.
intros. unfold In in H. contradiction.
intros. unfold In in H. destruct H.
rewrite H. simpl. destruct (eq_dec x x).
assert (length (remove eq_dec x p) <= length p).
apply remove_length.
auto with *.
tauto.
simpl. destruct (eq_dec x a). assert (length (remove eq_dec x p) <= length p).
apply remove_length.
auto with *.
simpl.
assert (length (remove eq_dec x p) < length p).
apply IHp. apply H. auto with *. Qed.

(** Anything other than what's removed from the list is still there. *)
Theorem remove_preserves_in:
  forall p,
    forall x y: A,
        In y p -> y <> x -> In y (remove eq_dec x p).
induction p.
intros. simpl in H. contradiction.
intros. simpl. destruct (eq_dec x a). apply IHp.
destruct H. rewrite<- H in H0. rewrite e in H0. tauto.
exact H. exact H0. simpl. destruct H. left. assumption.
right. apply IHp. exact H. exact H0. Qed.

(** Removing something that's not in the list doesn't change anything. *)
Theorem remove_not_in:
  forall x a,
    ~In x a -> remove eq_dec x a = a.
induction a.
simpl. trivial.
simpl. intros.
assert (remove eq_dec x a0 = a0).
  apply IHa.
  tauto.
rewrite H0.
destruct (eq_dec x a).
rewrite e in H.
destruct H.
left. trivial.
trivial. Qed.

(** Doesn't matter if you remove before concatenation or afterwards. *)
Theorem remove_app_commute:
  forall x a b,
    remove eq_dec x a ++ remove eq_dec x b = remove eq_dec x (a ++ b).
induction a.
(* base *) simpl. trivial.
(* step *) simpl. destruct (eq_dec x a). exact IHa.
intro.
assert ((a :: remove eq_dec x a0) ++ remove eq_dec x b = a :: (remove eq_dec x a0 ++ remove eq_dec x b)).
apply app_comm_cons.
rewrite H.
assert(remove eq_dec x a0 ++ remove eq_dec x b = remove eq_dec x (a0 ++ b)). apply IHa.
rewrite H0. trivial. Qed.

(** Removing an element is equivalent to filtering. *)
Theorem remove_as_filter:
  forall x a,
    remove eq_dec x a = filter (fun (y: A) => if eq_dec x y then false else true) a.
induction a.
simpl. trivial.
simpl. destruct (eq_dec x a). assumption. rewrite IHa. trivial. Qed.
(***********************************************************************)
(* last, removelast *)
(***********************************************************************)
(** ** Count *)

(** Counting something that's not in there results in 0. *)
Theorem count_not_in:
  forall x l,
    ~ In x l -> count_occ eq_dec l x = 0.
induction l.
intros.
simpl. trivial.
intros.
simpl.
destruct eq_dec.
rewrite e in H.
simpl in H.
tauto.
simpl in H.
tauto. Qed.

(** Counting something that is in the list leads to a positive number *)
Theorem count_in:
  forall x l,
    In x l -> count_occ eq_dec l x > 0.
induction l.
intros. simpl in H. contradiction.
intros. simpl in H. destruct H.
rewrite H. simpl. destruct eq_dec. auto with *. tauto.
assert (count_occ eq_dec l x > 0).
apply IHl. exact H.
simpl. destruct eq_dec. auto. exact H0. Qed. 


(** Removing an item decreases the length of the list by the amount of those items that were removed. *)
Theorem count_remove:
  forall x l,
    count_occ eq_dec l x + length (remove eq_dec x l) = length l.
induction l.
simpl. trivial.
simpl. destruct eq_dec. 
assert (S (count_occ eq_dec l x + length (remove eq_dec x l)) = S (length l)).
auto. assert(S (count_occ eq_dec l x) + length (remove eq_dec x l) = S (count_occ eq_dec l x + length (remove eq_dec x l))).
auto. rewrite e. rewrite if_eq. apply eq_trans with (y := S (count_occ eq_dec l x + length (remove eq_dec x l))). assumption. assumption.
simpl. assert (count_occ eq_dec l x + S (length (remove eq_dec x l)) = S (count_occ eq_dec l x + length (remove eq_dec x l))).
auto. rewrite if_not_eq. simpl. rewrite H. rewrite IHl. trivial. apply not_eq_sym. assumption. Qed.

(** Removing a value does not affect counting other values. *)
Theorem remove_preserves_count:
  forall x y l,
    x <> y -> count_occ eq_dec l x = count_occ eq_dec (remove eq_dec y l) x.
induction l.
intros. simpl. trivial.
intros. simpl. destruct eq_dec. rewrite_all e. destruct eq_dec. symmetry in e0. contradiction.
simpl. destruct eq_dec. auto. tauto. destruct eq_dec. auto. simpl. destruct eq_dec.
rewrite_all e. tauto. auto. Qed.

(** Definition of count_occ through filter() and length(). *)
Theorem count_as_filter:
  forall x a,
    count_occ eq_dec a x = length (filter (fun (y: A) => if eq_dec x y then true else false) a).
induction a.
simpl. trivial.
simpl. destruct (eq_dec x a). simpl. rewrite IHa. trivial. rewrite e. rewrite if_eq. trivial. 
rewrite if_not_eq. assumption. apply not_eq_sym. assumption. Qed.

(** Counting in the concatenation by adding two counts *) 
Theorem count_app:
  forall x a b,
    count_occ eq_dec (a ++ b) x = count_occ eq_dec a x + count_occ eq_dec b x.
induction a. simpl. trivial.
simpl. destruct (eq_dec a x). simpl. intro. rewrite IHa. trivial.
apply IHa. Qed.

End Elts.

(***********************************************************************)

Section ListOps.

(* Reverse *)

(***********************************************************************)

(* Decidable equality *)

End ListOps.

(***********************************************************************)

(** ** Misc *)

Section Map.
  Variables A B : Type.
  Variable f : A -> B.

(** A variation on map_nth that checks that the index is in range. *) 
Theorem map_nth_in_range:
  forall l d d' n,
    n < length l ->
      nth n (map f l) d = f (nth n l d').
induction l. simpl map. intros. simpl in H. exfalso. assert(~(n < 0)). auto with arith. tauto.
intros. simpl map. destruct n. simpl. trivial. simpl.
apply IHl. simpl in H. auto with arith. Qed.

End Map.

(***********************************************************************)

(* Fold *)

(***********************************************************************)

(* List Power *)

(***********************************************************************)

Section Bool.
(* filter is here for some reason *)
Variable A: Type.
Variable f: A -> bool.

(** Filtering works as expected with concatenation. *)
Theorem filter_app:
  forall (a b: list A),
    filter f (a ++ b) = (filter f a) ++ (filter f b).
induction a. simpl. trivial.
intros. simpl. destruct (f a). rewrite IHa. auto.
apply IHa. Qed.

End Bool.

(***********************************************************************)

Section ListPairs.

End ListPairs.

(***********************************************************************)

Section length_order.
End length_order.

(***********************************************************************)

Section SetIncl.
End SetIncl.

(***********************************************************************)

Section Cutting.

Variable A: Type.

(** Alternate definition for firstn. The standard definition matches first n, then l. *)
Fixpoint firstn_alt (n:nat)(l:list A){struct l} : list A :=
  match l with
  | nil => nil
  | h :: t => match n with 
              | 0 => nil
              | S k => h :: (firstn_alt k t)
              end
  end.

(** The alternative definition of firstn is equivalent to the library one. *)
Theorem firstn_equiv:
  forall n l,
    firstn n l = firstn_alt n l.
induction n.
induction l.
simpl. trivial.
simpl. trivial.
simpl. intros.
destruct l. simpl. trivial.
simpl.
rewrite IHn. trivial. Qed.

(** Indexing the initial segment works properly provided that the index is in range. *)
Theorem firstn_nth:
  forall a (n k: nat) (d: A),
    k < n ->
      nth k a d = nth k (firstn n a) d.
intros.
assert(k >= length a \/ k < length a).
apply Lt.le_or_lt.
destruct H0.
assert(nth k a d = d).
apply(nth_overflow). auto with arith.
assert(nth k (firstn n a) d = d).
apply(nth_overflow).  rewrite firstn_length.
assert(min n (length a) <= k <-> n <= k \/ (length a) <= k).
apply(min_le_iff).
apply H2. right. auto with arith.
rewrite H1. rewrite H2. trivial.
assert((firstn n a) ++ (skipn n a) = a).
apply(firstn_skipn).
rewrite<- H1 at 1.
apply(app_nth1).
rewrite(firstn_length).
apply min_glb_lt. assumption. assumption. Qed.

(** Decomposition of trailing segment into head and tail.
 This is sort of an inductive property but reversed.
 *)
Theorem skipn_decompose:
    forall a (n: nat) (d: A),
      n < length a ->
        skipn n a = (nth n a d) :: (skipn (S n) a).
induction a.
simpl. intros. assert (~ (n < 0)). auto with *. contradiction.
simpl. intros. destruct n. simpl. trivial.
simpl. assert(n < length a0). auto with arith.
apply IHa with (d:=d) in H0.
rewrite H0. simpl. trivial. Qed.

End Cutting.

(***********************************************************************)

Section ReDun.
End ReDun.

(***********************************************************************)

Section NatSeq.

(** ** Iota and seq *)

(** Iota n is the 0...(n-1) sequence. *)
Definition iota (n:nat) := seq 0 n.

(** Items of a sequence are bound from below by its start. *)
Theorem seq_lower_bound:
    forall start len i: nat,
        In i (seq start len) -> i >= start.
induction start.
(* base *)
intros.
auto with *.
(* step *)
intros.
assert (In i (map S (seq start len))).
rewrite seq_shift. exact H.
assert (exists j, S j = i /\ In j (seq start len)).
apply in_map_iff. exact H0.
destruct H1.
destruct H1.
rewrite<- H1.
assert (x >= start).
apply IHstart with len. exact H2.
auto with *. Qed.

(** Head of a sequence. *)
Theorem seq_head:
    forall start len head tail,
        head :: tail = seq start len -> head = start.
destruct len. intros. discriminate H.
intros. injection H. intros. exact H1. Qed.

(** Tail of a sequence. *)
Theorem seq_tail:
    forall start len head tail,
        head :: tail = seq start len -> tail = seq (S start) (len - 1).
destruct len. intros. discriminate H.
intros. injection H. intros.
assert (S len - 1 = len).
unfold minus. fold minus.
auto with *.
rewrite H2. exact H0. Qed.  

(** Head of a iota is 0. *)
Theorem iota_head:
    forall n head tail,
        head :: tail = iota n -> head = 0.
unfold iota.
apply seq_head. Qed.

(** Tail of a iota through map S. *)
Theorem iota_tail:
    forall n head tail,
        head :: tail = iota n -> tail = map S (iota (n - 1)).
unfold iota.
intros.
assert (tail = seq 1 (n - 1)). apply seq_tail with head. exact H.
rewrite seq_shift. exact H0. Qed.

(** Length of a iota *)
Theorem iota_length:
    forall n,
        length (iota n) = n.
unfold iota.
intros.
apply seq_length. Qed.

(** Indexing iota with a range check. *)
Theorem iota_nth_in_range:
    forall n len d,
        n < len -> nth n (iota len) d = n.
unfold iota. intros. apply seq_nth. exact H. Qed.

(** Indexing iota with a default. *)
Theorem iota_nth:
    forall n len,
        nth n (iota len) n = n.
intros.
assert(len <= n \/ n < len).
apply Lt.le_or_lt.
destruct H.
apply nth_overflow.
rewrite iota_length. assumption.
apply iota_nth_in_range. assumption. Qed.

(** Upper bound on the iota items.
 This theorem works both ways.
 *)
Theorem iota_upper_bound:
    forall n i: nat,
        In i (iota n) <-> i < n.
Proof.
        assert (forall n i: nat,
                In i (iota n) -> i < n) as iota_upper_bound_right.
          unfold iota.
          induction n.
          unfold seq. unfold In. contradiction.
          unfold seq. fold seq. rewrite<- seq_shift. intros.
          destruct H. rewrite <- H.
          auto with *.
          assert (exists j, S j = i /\ In j (seq 0 n)).
          apply in_map_iff. apply H. destruct H0. destruct H0.
          rewrite<- H0.
          assert (x < n).
          apply IHn. exact H1.
          auto with *.
        

        assert(forall n i: nat,
                i < n -> In i (iota n)) as iota_upper_bound_left.
          intros.
          rewrite<- iota_nth with (len:=n) (n:=i).
          apply nth_In.
          rewrite iota_length.
          assumption.

intros.
split.
apply iota_upper_bound_right.
apply iota_upper_bound_left.
Qed.
 
(** Upper bound of a sequence. *)
Lemma seq_upper_bound:
    forall start len i: nat,
        In i (seq start len) -> i < start + len.
induction start.
apply iota_upper_bound.
intros.
rewrite<- seq_shift in H. apply in_map_iff in H.
destruct H. destruct H.
apply IHstart in H0.
rewrite <- H.
assert (S start + len = S (start + len)).
trivial.
rewrite H1.
auto with *.
Qed. 



End NatSeq.

(***********************************************************************)

(* Exists and Forall *)

(***********************************************************************)

(* simpl_list, ssimpl_list *)

(***********************************************************************)

(* vim: set ft=coq: *)
