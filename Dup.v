(* Here we do a little bit of permutations on containers. *)

Require Import List List1.
Require Lt.
Section Dup.

Variable A: Type.

Definition HasDup (l: list A) := 
  exists a: list A,
  exists b: list A,
  exists c: list A,
  exists x: A,
    l = a++x::b++x::c. 

Theorem nodup_equiv:
  forall l: list A,
    NoDup l <-> ~ HasDup l.
split. unfold not. intros. 
unfold HasDup in H0. destruct H0. destruct H0. destruct H0. destruct H0.
assert(~In x2 (x ++ x0 ++ x2 :: x1)). apply NoDup_remove_2. rewrite<- H0. assumption.
apply H1. rewrite app_assoc. apply in_or_app. right. auto with *.

intros. induction l. constructor. constructor. intro. apply H.
unfold HasDup.
assert(exists b, exists c, l = b ++ a :: c).
apply in_split. assumption. destruct H1. destruct H1.
rewrite H1. exists nil. exists x. exists x0. exists a.
simpl. trivial. apply IHl. unfold not. intros.
apply H. unfold HasDup in H0. destruct H0. destruct H0. destruct H0. destruct H0.
unfold HasDup.
exists (a :: x). exists x0. exists x1. exists x2.
rewrite H0. simpl. trivial. Qed.

Theorem hasdup_cons:
  forall x: A,
  forall l: list A,
    HasDup l -> HasDup (x :: l).
unfold HasDup.
intros. destruct H. destruct H. destruct H. destruct H.
exists (x :: x0). exists x1. exists x2. exists x3. rewrite H. simpl. trivial.
Qed.

Hypothesis eq_dec : forall x y: A, {x = y}+{x <> y}.

Theorem dirichlet:
  forall a b: list A,
    (forall x: A,
       In x a -> In x b) ->
    length a > length b ->
      HasDup a.
induction a. intros. simpl in H0. assert (~ (0 > length b)). auto with arith. contradiction.
(* induction step *)
intros.
remember (remove eq_dec a a0) as a'.
remember (remove eq_dec a b) as b'.
assert(In a b) as a_in_b.
  apply H. simpl. left. trivial.
assert(length b' < length b) as b_descent.
  rewrite Heqb'. apply remove_lowers_length. exact a_in_b. 
assert({In a a0} + {~In a a0}). apply in_dec. apply eq_dec.
case H1. intro a_dups. rewrite in_decompose in a_dups.
destruct a_dups. destruct H2. rewrite H2.
exists nil. exists x. exists x0. exists a. simpl. trivial.
apply eq_dec.
intro a_not_there.
assert(forall x : A, In x a0 -> In x b) as a0_subset_b.
intros.
apply H. simpl. right. assumption.
assert(forall x : A, In x a0 -> In x b') as a0_subset_b'.
intros. rewrite Heqb'.
apply remove_preserves_in. apply a0_subset_b. assumption. 
hnf. intro. rewrite H3 in H2. contradiction.
assert(length a0 > length b').
  simpl in H0. assert(length a0 >= length b). auto with arith.
  apply Lt.lt_le_trans with (m:=length b). assumption. auto with arith.
assert(HasDup a0).
  apply IHa with (b:=b'). assumption. assumption.
apply hasdup_cons. assumption. Qed.

End Dup.
