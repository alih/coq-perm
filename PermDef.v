(* Author: Ilya Mezhirov
 * License: CC0
 *)
(** * Here we define a permutation and ensure that the definition makes sense. 
 *)

Require Import List List1.
Require Import Arith.

(** A list is a permutation if all nonnegative integers less than its length are in it. 
  Stuff like "only once", "and nothing else" 
    will be derived from this definition shortly.
 *)
Definition IsPermutation (p: list nat) :=
    forall n,
        n < length p -> In n p.


(** Cropping: removing the maximal item from a list.
   This is the descent step used in most of the proofs in this file.
 *)
Definition perm_crop (p: list nat) := remove eq_nat_dec (pred (length p)) p.

(*************************************************************)

(** Cropping a non-empty permutation reduces its length.
  A stronger version, adding "exactly by one", comes later. 
 *)
Lemma weak_perm_crop_length:
  forall p,
    IsPermutation p -> p = nil \/ length (perm_crop p) < length p.
intros.
unfold perm_crop.
assert (p = nil \/ pred (length p) < length p).
destruct p.
left. trivial.
right. simpl. auto. destruct H0. left. trivial.
unfold IsPermutation in H.
remember (pred (length p)) as k.
assert (In k p).
apply H. exact H0.
right.
assert (length (remove eq_nat_dec k p) < length p).
apply remove_lowers_length. exact H1. exact H2. Qed.

(** Cropped permutation is a permutation. *)
Lemma perm_crop_perm:
  forall p,
    IsPermutation p -> IsPermutation (perm_crop p).
destruct p.
unfold perm_crop. unfold remove. trivial.
unfold IsPermutation.
unfold perm_crop.
intros. apply remove_preserves_in. 
apply H.
assert(length (remove eq_nat_dec (pred (length (n :: p))) (n :: p)) <= length (n :: p)).
apply remove_length.
remember (length (remove eq_nat_dec (pred (length (n :: p))) (n :: p))) as k.
remember (length (n :: p)) as m.
apply lt_le_trans with (m := k). exact H0. exact H1.
assert(n :: p = nil \/ length (perm_crop (n :: p)) < length (n :: p)).
  apply weak_perm_crop_length.
  unfold IsPermutation.
  apply H.
destruct H1.
discriminate H1.
unfold perm_crop in H1.
remember (length (remove eq_nat_dec (pred (length (n :: p))) (n :: p))) as k.
assert(length(n::p) = S (length p)).
simpl. trivial.
rewrite_all H2.
assert(forall x, pred (S x) = x).
trivial.
rewrite_all H3.
assert (k <= length p).
auto with *.
assert (n0 < length p).
apply lt_le_trans with (m := k). exact H0. exact H4.
intuition.
rewrite H6 in H5.
apply lt_irrefl in H5. exact H5. Qed.

(** The items of a permutation are less than its length. *)
Theorem perm_bound:
  forall p,
    IsPermutation p -> forall k, In k p -> k < length p.
assert(forall n, forall p, length p <= n /\ IsPermutation p -> forall k, In k p -> k < length p).
induction n.
intros.
destruct H.
destruct p.
unfold In in H0. contradiction.
unfold length in  H. apply le_Sn_0 in H. contradiction.
(* induction step *)
intros. destruct H.
assert(length p < S n \/ length p = S n).
apply le_lt_or_eq. exact H. clear H.
destruct H2.
assert(length p <= n).
auto with *.
apply IHn.
split. assumption. assumption. assumption. 
assert(In k (perm_crop p) \/ k = n).
  unfold perm_crop.
  assert (pred(length p) = n).
  rewrite H.
  trivial.
  rewrite H2.
  assert (In k p -> k <> n -> In k (remove eq_nat_dec n p)).
  apply remove_preserves_in.
  assert (k = n \/ k <> n).
  decide equality.
  destruct H4.
  right. assumption.
  left. apply H3. assumption. assumption.
destruct H2. assert(k < length (perm_crop p)).
apply IHn. split.
assert(p = nil \/ length (perm_crop p) < length p).
apply weak_perm_crop_length. assumption. destruct H3. rewrite H3 in H. unfold length in H. discriminate.
assert (length (perm_crop p) < length p). assumption.
rewrite H in H3.
apply Gt.gt_S_le. exact H3.
apply perm_crop_perm. assumption. assumption.
assert(p = nil \/ length (perm_crop p) < length p).
apply weak_perm_crop_length. assumption. destruct H4. rewrite H4 in H. unfold length in H. discriminate.
apply lt_trans with (m := length (perm_crop p)). assumption. assumption. rewrite H.
rewrite H2.
auto.
intros. apply H with (n := length p). split. trivial. assumption. assumption. Qed.

(** Cropping the permutation reduces its length by one.
  Note: this works for an empty permutation courtesy of pred().
  This makes weak_perm_crop_length obsolete.
 *)
Theorem perm_crop_length:
  forall p,
    IsPermutation p -> length (perm_crop p) = pred (length p).
intros.
assert(IsPermutation (perm_crop p)).
apply perm_crop_perm. assumption.
assert(p = nil \/ length (perm_crop p) < length p).
apply weak_perm_crop_length. assumption.
destruct H1.
rewrite H1.
simpl. trivial.
(* non-0 case *)
assert (In (length (perm_crop p)) p).
unfold IsPermutation in H.
apply H. assumption.
assert (length p = S (pred (length p))).
apply S_pred with (m := length (perm_crop p)). assumption.
assert (length (perm_crop p) <= pred (length p)). rewrite H3 in H1. auto with *.
assert (length (perm_crop p) < pred (length p) 
    \/ length (perm_crop p) = pred (length p)).
apply le_lt_or_eq. assumption.
destruct H5. Focus 2. assumption.
remember (length (perm_crop p)) as k. clear H1 H4.
assert (In k (perm_crop p)).
  unfold perm_crop.
  apply remove_preserves_in.
  assumption.
  unfold not. intro. rewrite_all<- H1.
  apply lt_irrefl in H5. assumption.
assert(k < length(perm_crop p)). apply perm_bound. assumption. assumption.
rewrite<- Heqk in H4. apply lt_irrefl in H4. contradiction. Qed.

(** A permutation contains everything that it should exactly once. *)
Theorem perm_count:
  forall p,
    IsPermutation p -> forall k, k < length p -> count_occ eq_nat_dec p k = 1.
Proof.
    (* A permutation contains its maximal item exactly once. *)
    assert(
      forall p,
        IsPermutation p -> p <> nil -> count_occ eq_nat_dec p (pred (length p)) = 1)
    as weak_permutation_count.
    intros.
    assert (length (perm_crop p) = pred(length p)).
    apply perm_crop_length. assumption.
    assert (IsPermutation (perm_crop p)). apply perm_crop_perm. assumption.
    remember (pred (length p)) as k.
    assert (count_occ eq_nat_dec p k + length (remove eq_nat_dec k p) = length p).
    apply count_remove.
    unfold perm_crop in H1. rewrite<- Heqk in H1. rewrite H1 in H3.
    remember (count_occ eq_nat_dec p k) as m. clear Heqm.
    assert (length p > 0). destruct p. tauto. simpl. auto with *. 
    clear H1 H2 H0. remember (length p) as n. clear Heqn.
    assert(k < n). rewrite Heqk. apply lt_pred_n_n. trivial.
    assert(n = S k). rewrite Heqk. apply S_pred with (m := k). assumption.
    rewrite_all H1. clear H4 H0 H1.
    rewrite plus_comm in H3.
    assert (k + m = k + 1).
    rewrite H3.
    assert(k + 1 = 1 + k). apply plus_comm. rewrite H0. simpl. trivial. 
    apply plus_reg_l with (p := k). assumption.

assert(forall n p, length p = n -> IsPermutation p -> forall k, k < length p -> count_occ eq_nat_dec p k = 1).
induction n. intros. destruct p. simpl in H1. assert(~ k < 0). auto with arith. contradiction.
simpl in H. discriminate H.
intros.
assert (k <= n).
rewrite H in H1.
auto with *.
assert (k < n \/ k = n).
apply le_lt_or_eq. assumption.
destruct H3.
assert (count_occ eq_nat_dec p k = count_occ eq_nat_dec (perm_crop p) k).
apply remove_preserves_count. rewrite H. simpl. unfold not. intro. rewrite H4 in H3. apply lt_irrefl in H3. contradiction.
rewrite H4.
apply IHn with (p := perm_crop p) (k := k). 
assert (n = pred (S n)). simpl. trivial.
rewrite H5. rewrite<- H. apply perm_crop_length. assumption. apply perm_crop_perm. assumption.
assert (length (perm_crop p) = pred (length p)).
apply perm_crop_length. assumption.
rewrite H5. rewrite H. simpl. assumption.
(* k = n case *)
assert (n = pred (S n)). simpl. trivial.
rewrite<- H in H4.
rewrite_all H3.
rewrite H4.
apply weak_permutation_count.
assumption.
unfold not. intro. rewrite H5 in H. simpl in H. discriminate H.
intros. apply H with (n := length p). trivial. exact H0. exact H1. Qed.

(** Every item of a permutation can not be found in its initial segment.
  This is another way of saying that each element is unique.
 *)
Theorem perm_not_in_initial_segment:
  forall p,
    IsPermutation p -> 
      forall i, i < length p ->
        forall d, ~In (nth i p d) (firstn i p).
intros. intro.
assert(count_occ eq_nat_dec (firstn i p) (nth i p d) > 0).
apply count_occ_In. assumption. clear H1.
assert((firstn i p) ++ (skipn i p) = p).
apply firstn_skipn.
assert(In (nth i p d) (skipn i p)).
rewrite skipn_decompose with (d:=d).
simpl. left. trivial. assumption.
assert(count_occ eq_nat_dec (skipn i p) (nth i p d) > 0).
apply count_in. assumption.
assert(count_occ eq_nat_dec p (nth i p d) = count_occ eq_nat_dec (firstn i p) (nth i p d) + count_occ eq_nat_dec (skipn i p) (nth i p d)).
rewrite<- H1 at 1.
apply count_app.
assert(count_occ eq_nat_dec p (nth i p d) = 1).
apply(perm_count). assumption. apply perm_bound. assumption.
apply nth_In. assumption.
remember (count_occ eq_nat_dec (firstn i p) (nth i p d)) as x. clear Heqx.
remember (count_occ eq_nat_dec (skipn i p) (nth i p d)) as y. clear Heqy.
remember (count_occ eq_nat_dec p (nth i p d)) as z. clear Heqz.
assert(x >= 1). auto. assert(y >= 1). auto.
assert(1 <= x). auto. assert(1 <= y). auto.
assert(1 + 1 <= x + y). apply plus_le_compat. assumption. assumption.
rewrite<- H5 in H11. rewrite H6 in H11.
assert(1 <= 0).
auto with arith.
assert (1 > 0).
auto with arith.
assert(~(1 > 0)).
auto with arith.
contradiction. Qed.

(* vim: set ft=coq: *)
